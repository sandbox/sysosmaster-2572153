# README - OAuth2 Proxy #
## _Meta Information_ ##
 - [Drupal project Page](https://www.drupal.org/sandbox/sysosmaster/2572153)
 -
[Drupal Repository git](http://git.drupal.org/sandbox/sysosmaster/2572153.git)
 -
[D.o. Repository viewer](http://drupalcode.org/sandbox/sysosmaster/2572153.git)

## Table of Contents <a name="toc">##
 - [Intro](#intro)
 - [License](#license)
 - [Requirements](#requirements)
 - [Installation](#installation)
 - [Enabling / disabling notice]("#activate")
 - [Configuration](#configuration)
 - [Testing](#testing)
 - [Recommended Companion Modules](#recommendations)
 - [Security Implications](#security)
 - [Use cases](#useCases)
 - [Example config](#example)


## Intro <a name="intro"> ##
 Ever needed to request some data from an API that is secured with  OAuth 2?
 Did you wish you had a simple way to request data from it,

without all the difficulties that OAuth 2 entails?
 Or have you ever just wanted to check a request without jumping through all the
 OAuth2 'hoops'?

 Well now we have the OAuth2 proxy module.
 The OAuth 2 Proxy can do all that for you. It does all that pesky OAuth 2 stuff
 and lets you fly on with a clearly defined local endpoint that simply proxies
 the request for you.

 This module is ideal for use-cases like:
 - A local cache of a remote resource with [Redis](http://redis.io/).
 - a command and Control or monitoring environment that needs to keep track of a
   multitude of other services that employ OAuth2 as a security measure.
 - Any other access to a remote service point protected by OAuth2.

## License <a name="license"> ##
This module is licensed under the
[GPLv 2](http://www.gnu.org/licenses/gpl-2.0.txt)

    Drupal module: 'OAuth 2 Proxy'
    Copyright (C) 2015, 040lab B.V.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 See the [module page](https://drupal.org/project/oauth2_proxy) for
 contact details of the current maintainer.

## Requirements <a name="requirements"> ##
This module requires the following:
 1.  An OAuth 2 secured endpoint to connect with.
 1.  [Composer Manager](https://drupal.org/project/composer_manager)
   - This requires [Composer](https://getcomposer.org)
 1.  [cURL HTTP Request](https://drupal.org/project/chr)
   - This is used for its good [Curl](http://curl.haxx.se) detection which is
     the real requirement here.

## Installation <a name="installation"> ##
 The recommended method of installing this module is through drush,
 since it leverages the composer components directly without requiring
 additional steps.

 For example: `$ drush dl oauth2_proxy ; drush en oauth2_proxy`.

## Enabling / disabling  notice <a name="activate"> ##
 For security purposes this module 'forgets' all active Tokens when it is
 disabled. (except for the refresh tokens)

 This is so when the module is re-enabled all connections need to be
 re-authenticated. (get a fresh token)

## Configuration <a name="configuration"> ##
 The following Steps will assist you in configuring the module.

 - Enable the module (If you have not already done so ;) )
 - Go to the "admin/config/services/oauth2vpn" page of your drupal setup.
 - On the client Selection dialog either select 'New' for a new client or one of
   the currently present clients. Then press the "Next" button.
 - On the Client page you will see the following fields I will explain each of
   them shortly here.
   1.  [Client Name] - The name of the 'client' your are setting up.
       this name is also used as part of the URL later when you want to access
       the proxy.
   1.  [Authentication Type] - what 'type' of authentication mechanism do you
       want to use.
       This is part of the inner workings of OAuth and influences how the
       'access' token is requested (part of the URL for example or as a Form or
       as a Basic authentication exchange.)
   1.  [Response Type] - What sort of oAuth process is being used, for the
       moment only the 'code' flow is supported.
   1.  [Access Token Type] - Where to 'place' the access token when doing the
       request. This can be :
       - As part of the URL.
       - As a Bearer token in the header.
       - As a oauth token in the header.
       - as a MAC, or Message Authentication Code, in the header.
   1.  [Base URL] - Main part of the URL to request authentication on.
       (OAuth2 service)
   1.  [Authentication URL] - The URL to request authentication on.
       ('convert' the ClientId and ClientSecret into a Authorization Code).
   1.  [Token Request URL] - The URL to request the access token on.
       ('convert' a Authorization code into a access token).
   1.  [Client ID] - OAuth 2 Client ID (sometimes called App-ID)
   1.  [Client Secret] - OAuth 2 Client Secret (sometimes called App-Secret)
   1.  [scope] - OAuth 2 scope to request. this should be a space separated
       scope request to use like 'basic banana account email'.
   1.  [Request URL] - The URL to Redirect the request to by the proxy
       (excluding all parts after the client name that is present in the URL)
       so when we have a client set up as foo which has a Request URL of
       "http://bar/" the proxy will divert the request
       "<drupal site>/oauth2vpn/foo/request.json" to "http://bar/request.json".
       and the request "<drupal site>/oauth2vpn/foo/dir/super.xml" to
       "http://bar/dir/super.xml"
   1.  [Redirect URL] (the URL to redirect to as part of the authentication
       process.) - disabled,  could be used for a 2 legged OAuth 2 process in a
       future version.
   1.  [Allowed Ip's] - a JSON formatted array of allowed IP's to access this
       proxy client, for example ``["127.0.0.1","127.1.1.1"]``. This is the
       primary security measure in this module and assumes you have secured the
       server itself from malicious use.
 - After you "Save client" your client is saved and ready to be used on its
   local endpoint.  

## Testing <a name="testing"> ##
 After config you can test the workings of the OAuth secured endpoint.
 This can be tested as if there is no OAuth in place simply by doing the request
 on the local endpoint, like you would do on the remote endpoint just without
 the OAuth2 parts.

 for example when using Curl you can do the following:
 ```
  $ curl -v -l http://<drupal site>/oauth2vpn/foo/request.json
 ```
 - the `-v` option adds verbosity to the output (you can add up to 3 of them)
 - the `-l` flag is to follow redirects.

 this means it is now easier to test the workings of an OAuth 2 secured endpoint
 or API.

## Recommended Companion Modules <a name="recommendations"> ##
 This module has been developed to be used against an OAuth 2 secured drupal
 service endpoint.

 So on that Endpoint we recommend the following modules:
  - [OAuth2 Server](https://drupal.org/project/oauth2_server) The OAuth 2 Server
   module. (make sure you get version 7.x-1.3+3-dev or newer)
  - [Services](https://drupal.org/project/services) The Service module to do
  requests on.  (make sure you get version 7.x-3.12+5-dev or newer)

 Since this module strips some of the security that OAuth 2 provides we
 recommend that you use additional security modules on the server!

  - [Services API Key Authentication](https://www.drupal.org/node/2129831).
  This module adds the ability to add further security measures by using an
  api-key URL token to allow access (or use a specific user)

 To Utilize the exposed endpoint you can use one of the following module.
  - [Web service client](https://www.drupal.org/project/wsclient).
  - [Web Service Data](https://www.drupal.org/project/wsdata).

## Security Implications <a name="security"> ##
 This module purposely and deliberately dilutes some of the protection offered
 by OAuth 2 to enable non OAuth 2 aware clients and software to have the
 benefits of a OAuth 2 security layer.

 However this also means that anyone using this module **must** add additional
 security measures (or safe guards) on the server hosting the drupal site with
 this module enabled.

 Part of the protection in place is an IP filter, limiting the locations a
 request can be made from. use common sense when changing this to any value not
 part of the server!

 Other protections that can be used are :

  - IDS (or Intrusion Detection Systems)
  - Firewall
  - Dedicated Hardware (we strongly advise you to _**NOT**_ use this software on
     a VPS or other shared machine, where others could have access to the
     'localhost').
  - Some drupal modules can also assist see the
  [Recommended Companion Modules](#recommandations) section.
  - Client side certificates (not currently natively supported by the module),
  when used correctly, make it impossible for anyone without the certificate to
  use the endpoint.

## Use cases <a name="useCases"> ##
This module is designed for the following use-case. Its use is not limited to
it, but it can help you choose if this module is the one you should be using.

### Use Case ###
 the module must do and enable the following uses.
 - transparently proxy a request over OAuth2 to another drupal site that has a
 OAuth2 secured API or service.
 - It must allow the use of API-KEYs to do actions based on a service user.
 - It must allow [Redis](http://redis.io/) to be used as a caching back-end on
   the server.

## Example config <a name="example"> ##
Example config values follows.

 - [Client Name]         = test.
 - [Authentication Type] = AUTH_TYPE_FORM.
 - [Access Token Type]   = ACCESS_TOKEN_BEARER.
 - [Base URL]            = https://_Oauth 2 Server_.
 - [Authentication URL]  = https://_Oauth 2 Server_/oauth2/authorize.
 - [Token Request URL]   = https://_Oauth 2 Server_/oauth2/token.
 - [Client ID]           = idMe.
 - [Client Secret]       = thisIsMySuperSecret.
 - [scope]               = basic offline_access email profile.
 - [Request URL]         = https://_Oauth 2 Secured Endpoint_/.
 - [Allowed Ip's]        = ["127.0.0.1","127.1.1.1"]
